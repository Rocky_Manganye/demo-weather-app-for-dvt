﻿using System;
using Newtonsoft.Json;
using Models;

namespace Service.Core.Mappers
{
    public class WeatherMapper
    {
        public static Response MapperToResponse(string json)
        {
            try {
                var data = json.ToString();
                return JsonConvert.DeserializeObject<Response>(data);
            } catch(Exception e)
            {
               // Console.WriteLine(e.Message);
            }
            return null;
        }
    }
}