﻿using System;
using System.Net;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Models;
using Newtonsoft.Json;
using Service.Core.Constants;
using Service.Core.Mappers;

namespace Service.Service
{
    public class WeatherService : IWeatherService
    {
        private HttpClient _httpClient;

        public WeatherService()
        {
            _httpClient = new HttpClient();
        }

        public async void GetWeatherByCity(string city, Action<Response> callback)
        {
            var datapoint = $"{ServiceConstants.OPEN_WEATHER_BASE_URL}{""}{city}{ServiceConstants.OPEN_WEATHER_COUNTRY_AND_APPID}";

            var request = CreateRequest(datapoint);
            /*using (var response = await request.GetResponseAsync())
            {
                using (var stream = response.GetResponseStream())
                {
                    var jsonDoc = await Task.Run(() => JsonValue.Load(stream));
                    var weatherData = WeatherMapper.MapperToResponse(jsonDoc);
                    callback(weatherData);
                }
            }*/
        }

        public async void GetWeatherByLatLong(double latitude, double longitude, Action<Response> callback)
        {
            var datapoint = $"{ServiceConstants.OPEN_WEATHER_BASE_URL}{ServiceConstants.OPEN_WEATHER_GET_BY_LAT_ENDPOINT}{latitude}{ServiceConstants.OPEN_WEATHER_GET_BY_LON_ENDPOINT}{longitude}{ServiceConstants.OPEN_WEATHER_COUNTRY_AND_APPID}";

            var response = _httpClient.GetStringAsync(datapoint).Result;

            if (response != null)
            {
                var weatherData = JsonConvert.DeserializeObject<Response>(response);
                callback(weatherData);
            }
            else
            {
                callback(null);
            }
            /*var request = CreateRequest(datapoint);
            using (var response = await request.GetResponseAsync())
            {
                using (var stream = response.GetResponseStream())
                {
                    var jsonDoc = await Task.Run(() => JsonValue.Load(stream));
                    var weatherData = WeatherMapper.MapperToResponse(jsonDoc);
                    callback(weatherData);
                }
            }*/
        }

        private static HttpWebRequest CreateRequest(string datapoint)
        {
            var url = new Uri(datapoint);
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.ContentType = "application/json";
            request.Method = "GET";
            return request;
        }
    }
}