﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Models;

namespace Service.Service
{
    public interface IWeatherService
    {
        void GetWeatherByCity(string city, Action<Response> callback);
        void GetWeatherByLatLong(double latitude, double longitude, Action<Response> callback);
    }
}