﻿namespace Service.Core.Constants
{
    public class ServiceConstants
    {
        //http://samples.openweathermap.org/data/2.5/forecast?q=Sandton,ZA&appid=b1b15e88fa797225412429c1c50c122a1
        public const string OPEN_WEATHER_BASE_URL = "http://api.openweathermap.org/data/2.5/weather?";
        public const string OPEN_WEATHER_APPID = "83fe5035f647525319f346ac209acbd8";
        public const string OPEN_WEATHER_GET_BY_LAT_ENDPOINT = "lat=";
        public const string OPEN_WEATHER_GET_BY_LON_ENDPOINT = "&lon=";
        public const string OPEN_WEATHER_COUNTRY_AND_APPID = "&appid=83fe5035f647525319f346ac209acbd8";
    }
}