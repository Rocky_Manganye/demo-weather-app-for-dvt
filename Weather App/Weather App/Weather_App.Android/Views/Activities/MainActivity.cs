﻿using System;
using System.Globalization;
using Android;
using Android.App;
using Android.Content.PM;
using Android.Locations;
using Android.OS;
using Android.Views;
using Android.Widget;
using Com.Lilarcor.Cheeseknife;
using MvvmCross.Droid.Platform;
using MvvmCross.Droid.Support.V7.AppCompat;
using Square.Picasso;
using Weather_App.Android.Views.Utils;
using Weather_App.Core.ViewModels;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace Weather_App.Android.Views
{
    [Activity(
        Theme = "@style/Weather_App.Android",
        WindowSoftInputMode = SoftInput.AdjustPan | SoftInput.StateHidden)]
    public class MainActivity : MvxCachingFragmentCompatActivity<MainContainerViewModel> , ILocationListener
    {
        private LocationManager _locationManager;
        private ProgressDialog _progressDialog;

        [InjectView(Resource.Id.DateTextView)]
        private TextView _textDate;
        [InjectView(Resource.Id.max_temp)]
        private TextView _textMaxTemp;
        [InjectView(Resource.Id.min_text)]
        private TextView _textMinTemp;
        [InjectView(Resource.Id.location_text)]
        private TextView _textLocation;

        [InjectView(Resource.Id.weatherIconImageView)]
        private ImageView _image;

        protected Toolbar Toolbar => FindViewById<Toolbar>(Resource.Id.toolbar);

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.layout_activity);
            Cheeseknife.Inject(this);
            //Toolbar.Title = "Under the Weather";
            SetSupportActionBar(Toolbar);
            SupportActionBar.Title = "Under the Weather";
        }

        protected override void OnResume()
        {
            base.OnResume();
            _progressDialog = new ProgressDialog(this);
            _progressDialog.Indeterminate = true;
            _progressDialog.SetProgressStyle(ProgressDialogStyle.Spinner);
            _progressDialog.SetMessage("Please wait...");
            _progressDialog.SetCancelable(false);


            if (CheckSelfPermission(Manifest.Permission.AccessFineLocation) != Permission.Granted)
            {
                RequestPermissions(new[]
                {
                    Manifest.Permission.AccessFineLocation
                }, 100);
            }
            else
            {
                GetLocationDetails();
            }
        }

        private void GetLocationDetails()
        {
            RunOnUiThread(() =>
            {
                _progressDialog.Show();
            });

            _locationManager = GetSystemService(LocationService) as LocationManager;
            _locationManager.RequestLocationUpdates(_locationManager.GetBestProvider(new Criteria(), false), 1000 * 60,
                50, this);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions,
            Permission[] grantResults)
        {
            if (requestCode == 100)
            {
                if (grantResults[0] == Permission.Granted)
                {
                    GetLocationDetails();
                }
                else
                {
                    RequestPermissions(new[] { Manifest.Permission.AccessFineLocation }, 100);
                }
            }
        }

        public void OnLocationChanged(Location location)
        {
            ViewModel.GetWeatherByLocation(location.Latitude, location.Longitude, response =>
            {
                RunOnUiThread(() =>
                {
                    var imageUrl = $"http://download.spinetix.com/content/widgets/icons/weather/{response.weather[0].icon}.png";
                    Picasso.With(this)
                        .Load(imageUrl)
                        .NoPlaceholder()
                        .Into(_image);

                    _textDate.Text = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)
                        .AddSeconds(response.dt).ToLocalTime().ToString("D",
                            CultureInfo.CreateSpecificCulture("en-US"));

                    _textMaxTemp.Text = $"max {DegreesConverter.ToCelcius(response.main.temp_max).ToString(CultureInfo.InvariantCulture)} °C";


                    _textMinTemp.Text = $"min {DegreesConverter.ToCelcius(response.main.temp_min).ToString(CultureInfo.InvariantCulture)} °C";
                    _textLocation.Text = $"{response.name} , {response.sys.country}";

                    _progressDialog.Dismiss();
                });

            });
        }

        public void OnProviderDisabled(string provider)
        {
            Toast.MakeText(this, "OnProviderDisabled", ToastLength.Long).Show();
        }

        public void OnProviderEnabled(string provider)
        {
            Toast.MakeText(this, "OnProviderEnabled", ToastLength.Long).Show();
        }

        public void OnStatusChanged(string provider, Availability status, Bundle extras)
        {
            Toast.MakeText(this, "OnStatusChanged", ToastLength.Long).Show();
        }
    }
}
