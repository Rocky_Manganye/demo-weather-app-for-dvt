﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Weather_App.Android.Views.Utils
{
    public class DegreesConverter
    {

        public static double ToCelcius(double pDegrees) => pDegrees - 273.15D;
    }
}