﻿using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.IoC;
using Weather_App.Core.ViewModels;

namespace Weather_App.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            RegisterAppStart<MainContainerViewModel>();
        }
    }
}
