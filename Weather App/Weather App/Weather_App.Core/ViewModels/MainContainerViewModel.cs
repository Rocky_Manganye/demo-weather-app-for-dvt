﻿using System;
using Models;
using MvvmCross.Core.ViewModels;
using Service.Service;

namespace Weather_App.Core.ViewModels
{
    public class MainContainerViewModel : MvxViewModel
    {
        public void GetWeatherByLocation(double locationLatitude, double locationLongitude, Action<Response> action)
        {
            var service = new WeatherService();
            service.GetWeatherByLatLong(locationLatitude, locationLongitude, action);
        }
    }
}
