﻿using System.Collections.Generic;

namespace Models
{
    public class Response
    {
        public Coord coord { get; set; }
        public List<Weather> weather { get; set; }
        public string @base { get; set; }
        public Main main { get; set; }
        public long visibilty { get; set; }
        public Wind wind { get; set; }
        public Clouds clouds { get; set; }
        public long dt { get; set; }
        public Sys sys { get; set; }
        public string cod { get; set; }
        public int id { get; set; }
        public string name { get; set; }

    }
}